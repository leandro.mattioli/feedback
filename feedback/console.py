import io
import sys
import logging
import feedback
from feedback import logger

def process_many_to_many(callback, rules):
    """Process many inputs into many outputs."""
    for src, dest in rules:
        process_single(callback, src, dest)

def process_single(callback, src, dest):
    """Process a single file into a single output."""
    if feedback.VERBOSE and src != sys.stdin and dest != sys.stdout:
        logger.log(logging.INFO, "Processing %s into %s" % 
            (src, dest)
        )
    callback(src, dest)
        