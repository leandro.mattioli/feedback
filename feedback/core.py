from os import path
import logging

logger = logging.getLogger('feedback')

def replace_extension(filepath, ext='.out'):
    return path.splitext(filepath)[0] + ext