import csv

def load_csv(csvpath):
    """Load a list of dictionaries from a CSV file with a header row"""
    with open(csvpath, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        return [row for row in reader]